import { isAfter, isBefore } from 'date-fns';

// ф-я отвечает за проверку доступности выбранного времени для создания бронирования
// первые 2 даты это даты существующего бронирования, вторые 2 это даты нового бронирования

export function CheckAvaliableTimeHandler(
  start: Date,
  end: Date,
  compareStart: Date,
  compareEnd: Date,
): boolean {
  // если дата начала или дата конца новой брони попадает между датами
  // существующих броней аудитории, то надо выбрать другое время
  if (
    (isAfter(new Date(compareStart), start) &&
      isBefore(new Date(compareStart), end)) ||
    (isAfter(new Date(compareEnd), start) &&
      isBefore(new Date(compareEnd), end))
  ) {
    return false;
  }

  // если даты начала или даты конца броней совпадают, такая же ошибка
  if (new Date(compareStart) === start || new Date(compareEnd) === end) {
    return false;
  }

  return true;
}
