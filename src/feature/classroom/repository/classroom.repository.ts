import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../../../prisma/prisma.service';

@Injectable()
export class ClassroomRepository {
  constructor(private prisma: PrismaService) {}

  async getClassroom(id: number) {
    const classroom = await this.prisma.classroom.findUnique({ where: { id } });

    if (!classroom) return null;

    return classroom;
  }

  async createClassroom(data: any) {
    return this.prisma.classroom.create({
      data: { ...data },
      select: {
        id: true,
        number: true,
        seatsCount: true,
        pcCount: true,
        hasProjector: true,
        area: true,
        booking: true,
      },
    });
  }

  async updateClassroom(id: number, data: any) {
    await this.prisma.classroom.update({ where: { id }, data: { ...data } });
  }

  async deleteClassroom(id: number) {
    await this.prisma.classroom.delete({ where: { id } });
  }
}
