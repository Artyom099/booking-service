import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../../../prisma/prisma.service';
import { Classroom } from '@prisma/client';

@Injectable()
export class ClassroomQueryRepository {
  constructor(private prisma: PrismaService) {}

  async getClassroom(id: number): Promise<Classroom> {
    const classroom = await this.prisma.classroom.findUnique({
      where: { id },
      select: {
        id: true,
        number: true,
        seatsCount: true,
        pcCount: true,
        hasProjector: true,
        area: true,
        booking: true,
      },
    });

    if (!classroom) return null;

    return classroom;
  }

  async getClassrooms() {
    return this.prisma.classroom.findMany({
      select: {
        id: true,
        number: true,
        seatsCount: true,
        pcCount: true,
        hasProjector: true,
        area: true,
        booking: true,
      },
    });
  }

  // mapToViewer(classroom: Classroom): ClassroomViewModel {
  //   return {
  //     number: classroom.number,
  //     seatsCount: classroom.seatsCount,
  //     pcCount: classroom.pcCount,
  //     hasProjector: classroom.hasProjector,
  //     area: classroom.number,
  //     bookings: classroom.,
  //   };
  // }
}
