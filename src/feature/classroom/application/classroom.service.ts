import { Injectable, NotFoundException } from '@nestjs/common';
import { ClassroomRepository } from '../repository/classroom.repository';
import { ClassroomInput } from '../api/models/classroom.input';

@Injectable()
export class ClassroomService {
  constructor(private classroomRepository: ClassroomRepository) {}

  async getClassroom(id: number) {
    return this.classroomRepository.getClassroom(id);
  }

  async createClassroom(body: ClassroomInput) {
    return this.classroomRepository.createClassroom(body);
  }

  async updateClassroom(id: number, dto: ClassroomInput) {
    const classroom = await this.classroomRepository.getClassroom(id);
    if (!classroom) throw new NotFoundException('Classroom not found');

    return this.classroomRepository.updateClassroom(id, dto);
  }

  async deleteClassroom(id: number) {
    const classroom = await this.classroomRepository.getClassroom(id);
    if (!classroom) throw new NotFoundException('Classroom not found');

    return this.classroomRepository.deleteClassroom(id);
  }
}
