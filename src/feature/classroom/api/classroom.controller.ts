import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ClassroomService } from '../application/classroom.service';
import { ClassroomQueryRepository } from '../repository/classroom.query.repository';
import { ClassroomInput } from './models/classroom.input';

@ApiTags('classroom')
@Controller('classroom')
export class ClassroomController {
  constructor(
    private classroomService: ClassroomService,
    private classroomQueryRepository: ClassroomQueryRepository,
  ) {}

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  async getClassroom(@Param('id') id: string) {
    return this.classroomQueryRepository.getClassroom(parseInt(id));
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  async getClassrooms() {
    return this.classroomQueryRepository.getClassrooms();
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async createClassroom(@Body() body: ClassroomInput) {
    return this.classroomService.createClassroom(body);
  }

  @Put(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async updateClassroom(@Param('id') id: string, @Body() body: ClassroomInput) {
    return this.classroomService.updateClassroom(parseInt(id), body);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async deleteClassroom(@Param('id') id: string) {
    return this.classroomService.deleteClassroom(parseInt(id));
  }
}
