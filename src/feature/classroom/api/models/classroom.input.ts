import { ApiProperty } from '@nestjs/swagger';

export class ClassroomInput {
  @ApiProperty()
  number: string;
  @ApiProperty()
  seatsCount: number;
  @ApiProperty()
  pcCount: number;
  @ApiProperty()
  hasProjector: boolean;
  @ApiProperty()
  area: number;
}
