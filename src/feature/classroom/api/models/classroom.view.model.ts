import { ApiProperty } from '@nestjs/swagger';
import { BookingViewModel } from '../../../booking/api/models/booking.view.model';

export class ClassroomViewModel {
  @ApiProperty()
  number: number;
  @ApiProperty()
  seatsCount: number;
  @ApiProperty()
  pcCount: number;
  @ApiProperty()
  hasProjector: boolean;
  @ApiProperty()
  area: number;
  @ApiProperty()
  bookings: BookingViewModel[];
}
