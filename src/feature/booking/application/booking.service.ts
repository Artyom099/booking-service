import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { BookingRepository } from '../repository/booking.repository';
import { BookingInputModel } from '../api/models/booking.input.model';
import { ClassroomService } from '../../classroom/application/classroom.service';
import { isPast } from 'date-fns';
import { CheckAvaliableTimeHandler } from '../../../infrastructure/handlers/check.avaliable.time.handler';

@Injectable()
export class BookingService {
  constructor(
    private bookingRepository: BookingRepository,
    private classroomService: ClassroomService,
  ) {}

  async createBooking(dto: BookingInputModel) {
    const classroom = await this.classroomService.getClassroom(dto.classroomId);
    if (!classroom) throw new NotFoundException('Classroom not found');

    const bookings = await this.bookingRepository.getBookingsByClassroomId(
      dto.classroomId,
    );

    if (isPast(dto.startDate) || isPast(dto.endDate))
      throw new BadRequestException('This time is in the past');

    for (const booking of bookings) {
      if (
        CheckAvaliableTimeHandler(
          booking.startDate,
          booking.endDate,
          new Date(dto.startDate),
          new Date(dto.endDate),
        )
      ) {
        throw new BadRequestException('This time is busy, choose other time');
      }
    }

    return this.bookingRepository.createBooking(dto);
  }

  async updateBooking(id: number, dto: BookingInputModel) {
    const booking = await this.bookingRepository.getBooking(id);
    if (!booking) throw new NotFoundException('Booking not found');

    if (isPast(dto.startDate) || isPast(dto.endDate))
      throw new BadRequestException('This time is in the past');

    return this.bookingRepository.updateBooking(id, dto);
  }

  async deleteBooking(id: number) {
    const booking = await this.bookingRepository.getBooking(id);
    if (!booking) throw new NotFoundException('Booking not found');

    return this.bookingRepository.deleteBooking(id);
  }
}
