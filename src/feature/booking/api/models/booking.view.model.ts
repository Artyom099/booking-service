import { ApiProperty } from '@nestjs/swagger';

export class BookingViewModel {
  @ApiProperty()
  id: number;
  @ApiProperty()
  renter: string;
  @ApiProperty()
  startDate: string;
  @ApiProperty()
  endDate: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  classroomId: number;
}
