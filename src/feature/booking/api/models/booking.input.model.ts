import { ApiProperty } from '@nestjs/swagger';

export class BookingInputModel {
  @ApiProperty()
  renter: string;
  @ApiProperty()
  startDate: string;
  @ApiProperty()
  endDate: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  classroomId: number;
}
