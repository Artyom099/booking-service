import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { BookingService } from '../application/booking.service';
import { BookingQueryRepository } from '../repository/booking.query.repository';
import { BookingInputModel } from './models/booking.input.model';
import { BookingViewModel } from './models/booking.view.model';

@ApiTags('booking')
@Controller('booking')
export class BookingController {
  constructor(
    private bookingService: BookingService,
    private bookingQueryRepository: BookingQueryRepository,
  ) {}

  @ApiOperation({ summary: 'Get array of bookings' })
  @ApiOkResponse({ type: BookingViewModel })
  @Get(':id')
  @HttpCode(HttpStatus.OK)
  async getBooking(@Param('id') id: string) {
    return this.bookingQueryRepository.getBooking(parseInt(id));
  }

  @ApiOperation({ summary: 'Get booking by id' })
  @ApiOkResponse({ type: BookingViewModel })
  @Get()
  @HttpCode(HttpStatus.OK)
  async getBookings() {
    return this.bookingQueryRepository.getBookings();
  }

  @ApiOperation({ summary: 'Create booking' })
  @Post()
  @HttpCode(HttpStatus.CREATED)
  async createBooking(@Body() body: BookingInputModel) {
    return this.bookingService.createBooking(body);
  }

  @Put(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async updateBooking(
    @Param('id') id: string,
    @Body() body: BookingInputModel,
  ) {
    return this.bookingService.updateBooking(parseInt(id), body);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async deleteBooking(@Param('id') id: string) {
    return this.bookingService.deleteBooking(parseInt(id));
  }
}
