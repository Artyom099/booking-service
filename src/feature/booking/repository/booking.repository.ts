import { PrismaService } from '../../../../prisma/prisma.service';
import { Injectable } from '@nestjs/common';
import { BookingInputModel } from '../api/models/booking.input.model';

@Injectable()
export class BookingRepository {
  constructor(private prisma: PrismaService) {}

  async getBooking(id: number) {
    const booking = await this.prisma.booking.findUnique({ where: { id } });

    if (!booking) return null;

    return booking;
  }

  async getBookingsByClassroomId(classroomId: number) {
    const booking = await this.prisma.booking.findMany({
      where: { classroomId },
    });

    if (!booking) return null;

    return booking;
  }

  async createBooking(data: BookingInputModel) {
    return this.prisma.booking.create({
      data: { ...data },
      select: {
        id: true,
        renter: true,
        startDate: true,
        endDate: true,
        description: true,
        classroomId: true,
      },
    });
  }

  async updateBooking(id: number, data: BookingInputModel) {
    await this.prisma.booking.update({ where: { id }, data: { ...data } });
  }

  async deleteBooking(id: number) {
    return this.prisma.booking.delete({ where: { id } });
  }
}
