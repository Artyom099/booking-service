import { PrismaService } from '../../../../prisma/prisma.service';
import { Injectable, NotFoundException } from '@nestjs/common';

@Injectable()
export class BookingQueryRepository {
  constructor(private prisma: PrismaService) {}

  async getBooking(id: number) {
    const booking = await this.prisma.booking.findUnique({ where: { id } });

    if (!booking) throw new NotFoundException('Booking not found');

    return booking;
  }

  async getBookings() {
    return this.prisma.booking.findMany();
  }
}
