import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, HttpCode, HttpStatus } from '@nestjs/common';
import { ScheduleQueryRepository } from './schedule.query.repository';

@ApiTags('schedule')
@Controller('schedule')
export class ScheduleController {
  constructor(private scheduleQueryRepository: ScheduleQueryRepository) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  async getBookings() {
    return this.scheduleQueryRepository.getSchedule();
  }
}
