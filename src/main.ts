import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { setupSwagger } from './infrastructure/swagger/setup.swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  setupSwagger(app);

  await app.listen(3005, () => {
    console.log(`App started at http://localhost:${3005}`);
    console.log(`Swagger http://localhost:${3005}/api/v1/swagger`);
  });
}
bootstrap();
