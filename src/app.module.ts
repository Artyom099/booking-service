import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClassroomController } from './feature/classroom/api/classroom.controller';
import { ClassroomService } from './feature/classroom/application/classroom.service';
import { ClassroomRepository } from './feature/classroom/repository/classroom.repository';
import { ClassroomQueryRepository } from './feature/classroom/repository/classroom.query.repository';
import { PrismaService } from '../prisma/prisma.service';
import { BookingController } from './feature/booking/api/booking.controller';
import { BookingService } from './feature/booking/application/booking.service';
import { BookingRepository } from './feature/booking/repository/booking.repository';
import { BookingQueryRepository } from './feature/booking/repository/booking.query.repository';
import { ScheduleController } from './feature/schedule/schedule.controller';
import { ScheduleQueryRepository } from './feature/schedule/schedule.query.repository';

const services = [AppService, PrismaService, BookingService, ClassroomService];

const repositories = [
  BookingRepository,
  BookingQueryRepository,
  ClassroomRepository,
  ClassroomQueryRepository,
  ScheduleQueryRepository,
];

@Module({
  imports: [],
  controllers: [
    AppController,
    ClassroomController,
    BookingController,
    ScheduleController,
  ],
  providers: [...services, ...repositories],
})
export class AppModule {}
