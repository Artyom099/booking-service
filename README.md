<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="App Logo" /></a>
</p>

<p align="center">
    <img src="https://img.shields.io/badge/Nest-10.0.0-red" alt="Nest version" />
    <img src="https://img.shields.io/badge/Version-v1.0-green" alt="App version" />
    <img src="https://img.shields.io/badge/License-MIT-blue" alt="License" />
</p>

## Description

The Booking Service is a NestJS-based backend application designed to manage bookings 
for various services or resources. It provides a RESTful API for creating, updating, 
and canceling bookings, as well as retrieving booking information.

Project stack: Nest, Postgres

## Distribute

In progress

## Documentation

In progress

## Developers

- [Artyom Golubev](https://gitlab.com/Artyom099)

## License

This project is distributed under the [MIT license]()
