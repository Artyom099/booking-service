-- CreateExtension
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- CreateTable
CREATE TABLE "classroom" (
    "id" SERIAL NOT NULL,
    "number" INTEGER NOT NULL,
    "seatsCount" INTEGER NOT NULL DEFAULT 1,
    "pcCount" INTEGER NOT NULL DEFAULT 1,
    "hasProjector" BOOLEAN NOT NULL DEFAULT false,
    "area" INTEGER NOT NULL,

    CONSTRAINT "classroom_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "classroom_number_key" ON "classroom"("number");
